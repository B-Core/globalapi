const request = require('supertest');
const checkObjectModel = require('../helpers/checkObjectModel');
const app = require('../../build/app');

describe('GET /:userId', () => {
  it('should respond with Unauthorized if no credentials', (done) => {
    request(app)
      .get('/user')
      .expect(401)
      .end(done);
  });
  it('should respond with all the users and status 200', (done) => {
    request(app)
      .get('/user')
      .auth('Ciro', 'anneau3105')
      .expect('Content-Type', /json/)
      .expect((response) => {
        const user = response.body;
        if (response.status !== 200) {
          throw new Error(`Header = ${response.status}`);
        }
        checkObjectModel(user[0].doc, {
          _id: '',
          name: '',
          password: '',
          salt: '',
          type: '',
          certifiedClients: '',
          rights: '',
          roles: '',
          datas: '',
          createdAt: '',
        });
      })
      .end(done);
  });
  it('should respond with the user Ciro and status 200', (done) => {
    request(app)
      .get('/user?name=Ciro')
      .auth('Ciro', 'anneau3105')
      .expect('Content-Type', /json/)
      .expect((response) => {
        const user = response.body;
        if (response.status !== 200) {
          throw new Error(`Header = ${response.status}`);
        }
        checkObjectModel(user[0].doc, {
          name: 'Ciro',
          type: 'User',
          certifiedClients: [],
          rights: ['read', 'write'],
          roles: {
            Admin: {
              rights: '*',
              datas: '*',
            },
            Reader: {
              rights: 'read',
              datas: '*',
            },
          },
          datas: {},
        }, true);
      })
      .end(done);
  });
});
