const request = require('supertest');
const app = require('../../build/app');

describe('GET /', () => {
  it('should greet us with a welcome message', (done) => {
    request(app)
      .get('/')
      .expect('Content-Type', /text/)
      .expect(200, 'Welcome to the Echafaudeur.GlobalAPI')
      .end((err) => {
        if (err) throw err;
        done();
      });
  });
});
