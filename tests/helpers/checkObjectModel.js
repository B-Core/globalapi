const checkObject = (object, model, key) => {
  for (const k in object[key]) {
    if (model[key][k] instanceof Object) return checkObject(object[key], model[key], k);
    if (!object[key][k] || object[key][k] !== model[key][k]) throw new Error(`expected key ${k} with value ${object[key][k]} to have value ${model[key][k]}`);
  }
  return true;
};

const checkObjectModel = (object, model, values = false) => {
  for (const key in model) {
    if (values) {
      if (model[key] instanceof Array) {
        if (!object[key] || object[key].join(',') !== model[key].join(',')) throw new Error(`expected key ${key} with value ${object[key]} to have value ${model[key]}`);
      } else if (model[key] instanceof Object) {
        if (object[key]) {
          checkObject(object, model, key);
        }
      } else if (!object[key] || object[key] !== model[key]) throw new Error(`expected key ${key} with value ${object[key]} to have value ${model[key]}`);
    } else if (!object[key]) throw new Error(`missing key ${key} in ${JSON.stringify(object)}`);
  }
};

module.exports = checkObjectModel;
