import babel from 'rollup-plugin-babel';

export default {
  entry: 'src/app.js',
  format: 'cjs',
  moduleName: 'GlobalAPI',
  dest: 'build/app.js',
  sourceMap: true,
  plugins: [babel()],
};
