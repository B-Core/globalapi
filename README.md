JWT Authentication API and PouchDB Database Server.

# Installation

### required
+ nodejs ^6.0
+ git
+ [CouchDB](https://couchdb.apache.org/)

### process
First clone the repository
```
git clone https://B-Core@bitbucket.org/B-Core/globalapi.git
```
Then install the dependencies with `npm` (it should come with node installation)
```
// in the project directory
npm install
```

## commands
+ start the server: `npm start`
+ watch the server: `npm run watch-server` (need supervisor installed with -g flag)
+ watch build process: `npm run watch`
+ build: `npm run build`
+ test: `npm test`

# Architecture
The project is built with [rollup](http://rollupjs.org/) to allow the use of ES6 newest syntax and specifically ES6 import. So all the _running_ code will be in the `build/` folder and will consist of one big `app.js` file.

## components
> components are the basic building blocks of the application. This is typically where you will put your classes

## config
> for the config files

## routes
> contains all the routes for the application. Routes in an Express server are a string representing the `url` and a `function` corresponding to the action to do when reaching this route.
The middleware folder is for all the __custom__ middlewares. those form the node module part can be imported directly in the `router.js` file.

## app.js
> Application entry point.

# Dependencies
+ body-parser: ^1.16.0
+ connect-ensure-login: ^0.1.1
+ cookie-parser: ^1.4.3
+ cors: ^2.8.1
+ express: ^4.14.1
+ express-session: ^1.15.0
+ hbs: ~4.0.1
+ helmet: ^3.4.0
+ morgan: ^1.8.0
+ node-sass-middleware: 0.9.8
+ passport: ^0.3.2
+ passport-http: ^0.3.0
+ passport-http-bearer: ^1.0.1
+ passport-local: ^1.0.0
+ pouchdb: ^6.1.2
+ rollup-plugin-babel: ^2.7.1
+ rollup-plugin-buble: ^0.15.0
+ rollup-plugin-uglify: ^1.0.1
+ serve-favicon: ~2.3.2
+ uuid: ^3.0.1
+ rollup: ^0.41.4
+ rollup-watch: ^3.2.2
+ fs-extra: ^2.0.0
+ handlebars: ^4.0.6
+ jsonwebtoken: ^7.3.0

# dev Dependencies
+ babel-preset-es2015: ^6.22.0
+ chai: *
+ css-loader: ^0.23.1
+ eslint: ^3.13.0
+ eslint-config-airbnb: ^14.0.0
+ eslint-config-airbnb-base: ^11.0.1
+ eslint-loader: ^1.3.0
+ eslint-plugin-import: ^2.2.0
+ eslint-plugin-jsx-a11y: ^3.0.2
+ eslint-plugin-react: ^6.9.0
+ mocha: ^3.2.0
+ supertest: ^3.0.0
