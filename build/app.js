'use strict';

var config = {
  APP_PORT: 3570,
  DB_HOST: 'http://localhost:5984'
};

const express$1 = require('express');

const router$1 = new express$1.Router();

router$1.get('/', (request, response) => {
  response.send('Welcome to the Echafaudeur.GlobalAPI');
});

const uuid = require('uuid/v1'); // v1 = time based, v4 = random
const uuidRnd = require('uuid/v4');
const passport$1 = require('passport');
const crypto = require('crypto');

class Client {
  constructor(params) {
    const model = {
      name: params.name,
      password: params.password,
      role: params.role || 'Reader',
      salt: params.salt || uuidRnd(),
      certifiedClients: params.certifiedClients || [],
      rights: params.rights || [],
      roles: params.roles || {
        Admin: {
          rights: '*',
          datas: '*'
        },
        Reader: {
          rights: 'read',
          datas: '*'
        }
      },
      datas: params.datas || {},
      createdAt: params.createdAt || new Date(),
      id: params.id || uuid()
    };
    if (typeof model.name !== 'string') throw new Error('name must be a string');
    for (const key in model) {
      this[key] = model[key];
    }

    const cipher = crypto.createCipher('aes-256-cbc', this.salt);

    cipher.update(this.password, 'utf8', 'base64');
    this.password = cipher.final('base64');
  }

  isTokenACertifiedClient(token) {
    for (let clientIndex = 0; clientIndex < this.certifiedClients.length; clientIndex++) {
      const client = this.certifiedClients[clientIndex];
      if (client.token === token) {
        return true;
      }
    }
    return false;
  }
  isCertifiedClient(clientID) {
    return this.certifiedClients.filter(client => client.id === clientID).length === 1;
  }
  addCertifiedClient(client) {
    const index = this.certifiedClients.indexOf(client);
    if (index < 0) {
      return this.certifiedClients.push(client);
    }
    return this.certifiedClients[index];
  }
  removeCertifiedClient(client) {
    const index = this.certifiedClients.indexOf(client);
    if (index < 0) {
      return { error: `no client for ${client} in ${this.name}|id:${this._id}` };
    }
    return this.certifiedClients[index];
  }
  toJSON() {
    return {
      name: this.name,
      password: this.password,
      role: this.role,
      salt: this.salt,
      certifiedClients: this.certifiedClients,
      rights: this.rights,
      roles: this.roles,
      datas: this.datas,
      createdAt: this.createdAt,
      id: this.id
    };
  }
}

class User extends Client {
  constructor(params) {
    super(params);
    this.type = 'User';
  }
}

class Builder {
  constructor(models) {
    this.models = models;
  }
  create(model, ressource) {
    return new this.models[model](ressource);
  }
}

const models = {
  Client,
  User
};
const builder = new Builder(models);

const PouchDB = require('pouchdb');

class DataAccessor {
  constructor(databases, REMOTE_URI, remote) {
    if (!(databases instanceof Array)) throw new Error('databases must be an Array');

    this.remote = remote;

    this.databases = databases;
    this.REMOTE_URI = REMOTE_URI;
    this.pouchDatabases = this.mode();
    this.setPouchDebug(true);
  }

  createRessource(type, ressource) {
    const data = builder.create(type, ressource);
    return data;
  }
  mode() {
    const pouchDatabases = this.pouchDatabases || {};
    this.databases.forEach(name => {
      if (!pouchDatabases[name]) {
        const dbName = this.remote ? `${this.REMOTE_URI}/${name}` : `${name}`;
        pouchDatabases[name] = new PouchDB(dbName);
      }
    });
    return pouchDatabases;
  }
  database(databaseName) {
    if (!this.pouchDatabases[databaseName]) {
      throw new Error('DataBase does not exist');
    }
    return this.pouchDatabases[databaseName];
  }
  search(database, queries, callback) {
    this.database(database).allDocs({
      include_docs: true
    }).then(datas => {
      const docs = datas.rows;
      const querieLength = Object.keys(queries).length;
      const results = [];
      for (let dataIndex = 0; dataIndex < docs.length; dataIndex++) {
        let keyCount = 0;
        for (const key in queries) {
          // console.log('value: ', key, docs[dataIndex].doc[key]);
          // console.log('querie: ', key, queries[key]);
          if (!docs[dataIndex].doc[key] || docs[dataIndex].doc[key] !== queries[key]) break;
          keyCount++;
        }
        if (keyCount < querieLength) continue;
        results.push(docs[dataIndex]);
      }
      // console.log(results);
      callback(results);
    }).catch(error => {
      callback(error);
    });
  }
  setRemote(remote) {
    this.remote = remote;
    return this.remote;
  }
  setPouchDebug(activated = true) {
    activated ? PouchDB.debug.enable('*') : PouchDB.debug.disable();
  }
}

const dataAccessor = new DataAccessor(['users', 'clients', 'accesstokens'], config.DB_HOST, true);

const express$2 = require('express');

const router$2 = new express$2.Router();

router$2.get('/:model', (request, response) => {
  const model = request.params.model;
  const filters = request.query;
  dataAccessor.search(model, filters, datas => {
    response.json(datas);
  });
});
router$2.post('/:model', (request, response) => {
  const model = request.params.model;
  const type = `${model.toUpperCase()[0]}${model.slice(1, model.length - 1)}`;
  const data = dataAccessor.createRessource(type, request.body);
  dataAccessor.database(model).post(data, (error, datas) => {
    if (error) response.send(error.status, error);
    response.json(datas);
  });
});
router$2.put('/:model', (request, response) => {
  const model = request.params.model;
  const filters = request.query;
  dataAccessor.search(model, filters, datas => {
    if (datas.length === 1) {
      const hydratedBody = datas[0];
      for (const key in request.body) {
        hydratedBody.doc[key] = request.body[key];
      }
      dataAccessor.database(model).put(hydratedBody.doc, (error, datas2) => {
        if (error) response.send(error.status, error);
        response.json(datas2);
      });
    } else {
      response.send(`${model} with name ${filters.name} does not exist`);
    }
  });
});
router$2.delete('/:model', (request, response) => {
  const model = request.params.model;
  const filters = request.query;
  dataAccessor.search(model, filters, datas => {
    if (datas.length === 1) {
      const hydratedBody = datas[0].doc;
      dataAccessor.database(model).remove(hydratedBody, (error, datas2) => {
        if (error) response.send(error.status, error);
        response.json(datas2);
      });
    } else {
      response.send(`${model} with name ${filters.name} does not exist`);
    }
  });
});

const express$3 = require('express');
const passport$2 = require('passport');

const router$3 = new express$3.Router();

router$3.get('/', (request, response) => {
  response.render('login');
});

router$3.post('/', passport$2.authenticate('local', { successReturnToOrRedirect: '/', failureRedirect: '/login' }));

const express$4 = require('express');

const router$4 = new express$4.Router();

router$4.get('/', (request, response) => {
  request.logout();
  response.redirect('/');
});

const morgan = require('morgan')('combined');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors')();
const session = require('express-session')({
  secret: 's3Cur3',
  name: 'sessionId',
  resave: true,
  saveUninitialized: true
});
const helmet = require('helmet');
const passport = require('passport');
const sassMiddleware = require('node-sass-middleware');

var router = (app => {
  app.use(helmet());
  app.use(morgan);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(cookieParser());
  app.use(sassMiddleware({
    src: 'src/style',
    dest: 'public/style',
    indentedSyntax: true,
    sourceMap: true
  }));
  app.use(cors);
  app.use(session);
  app.use(passport.initialize());
  app.use(passport.session());

  app.use('/', router$1);
  app.use('/login', router$3);
  app.use('/logout', router$4);
  // app.get('/dialog/authorize' /* TODO */);
  // app.post('/oauth/token' /* TODO */);
  //
  // app.use(/* TODO authentication test */);
  app.use('/api', router$2);

  app.listen(config.APP_PORT, () => {
    console.log(`Server listen on port ${config.APP_PORT}`);
  });
});

const express = require('express');

const app = express();

// view engine setup
app.set('views', 'views');
app.set('view engine', 'hbs');

app.use('/static', express.static('public'));
router(app);

module.exports = app;
//# sourceMappingURL=app.js.map
