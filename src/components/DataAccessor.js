import Client from './Client';
import User from './User';
import Builder from './Builder';

const models = {
  Client,
  User,
};
const builder = new Builder(models);

const PouchDB = require('pouchdb');

export default class DataAccessor {
  constructor(databases, REMOTE_URI, remote) {
    if (!(databases instanceof Array)) throw new Error('databases must be an Array');

    this.remote = remote;

    this.databases = databases;
    this.REMOTE_URI = REMOTE_URI;
    this.pouchDatabases = this.mode();
    this.setPouchDebug(true);
  }

  createRessource(type, ressource) {
    const data = builder.create(type, ressource);
    return data;
  }
  mode() {
    const pouchDatabases = this.pouchDatabases || {};
    this.databases.forEach((name) => {
      if (!pouchDatabases[name]) {
        const dbName = this.remote ? `${this.REMOTE_URI}/${name}` : `${name}`;
        pouchDatabases[name] = new PouchDB(dbName);
      }
    });
    return pouchDatabases;
  }
  database(databaseName) {
    if (!this.pouchDatabases[databaseName]) {
      throw new Error('DataBase does not exist');
    }
    return this.pouchDatabases[databaseName];
  }
  search(database, queries, callback) {
    this.database(database).allDocs({
      include_docs: true,
    }).then((datas) => {
      const docs = datas.rows;
      const querieLength = Object.keys(queries).length;
      const results = [];
      for (let dataIndex = 0; dataIndex < docs.length; dataIndex++) {
        let keyCount = 0;
        for (const key in queries) {
          // console.log('value: ', key, docs[dataIndex].doc[key]);
          // console.log('querie: ', key, queries[key]);
          if (!docs[dataIndex].doc[key] || docs[dataIndex].doc[key] !== queries[key]) break;
          keyCount++;
        }
        if (keyCount < querieLength) continue;
        results.push(docs[dataIndex]);
      }
      // console.log(results);
      callback(results);
    }).catch((error) => {
      callback(error);
    });
  }
  setRemote(remote) {
    this.remote = remote;
    return this.remote;
  }
  setPouchDebug(activated = true) {
    activated ? PouchDB.debug.enable('*') : PouchDB.debug.disable();
  }
}
