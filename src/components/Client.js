const uuid = require('uuid/v1'); // v1 = time based, v4 = random
const uuidRnd = require('uuid/v4');
const passport = require('passport');
const crypto = require('crypto');

export default class Client {
  constructor(params) {
    const model = {
      name: params.name,
      password: params.password,
      role: params.role || 'Reader',
      salt: params.salt || uuidRnd(),
      certifiedClients: params.certifiedClients || [],
      rights: params.rights || [],
      roles: params.roles || {
        Admin: {
          rights: '*',
          datas: '*',
        },
        Reader: {
          rights: 'read',
          datas: '*',
        },
      },
      datas: params.datas || {},
      createdAt: params.createdAt || new Date(),
      id: params.id || uuid(),
    };
    if (typeof model.name !== 'string') throw new Error('name must be a string');
    for (const key in model) {
      this[key] = model[key];
    }

    const cipher = crypto.createCipher('aes-256-cbc', this.salt);

    cipher.update(this.password, 'utf8', 'base64');
    this.password = cipher.final('base64');
  }

  isTokenACertifiedClient(token) {
    for (let clientIndex = 0; clientIndex < this.certifiedClients.length; clientIndex++) {
      const client = this.certifiedClients[clientIndex];
      if (client.token === token) {
        return true;
      }
    }
    return false;
  }
  isCertifiedClient(clientID) {
    return this.certifiedClients.filter(client => client.id === clientID).length === 1;
  }
  addCertifiedClient(client) {
    const index = this.certifiedClients.indexOf(client);
    if (index < 0) {
      return this.certifiedClients.push(client);
    }
    return this.certifiedClients[index];
  }
  removeCertifiedClient(client) {
    const index = this.certifiedClients.indexOf(client);
    if (index < 0) {
      return { error: `no client for ${client} in ${this.name}|id:${this._id}` };
    }
    return this.certifiedClients[index];
  }
  toJSON() {
    return {
      name: this.name,
      password: this.password,
      role: this.role,
      salt: this.salt,
      certifiedClients: this.certifiedClients,
      rights: this.rights,
      roles: this.roles,
      datas: this.datas,
      createdAt: this.createdAt,
      id: this.id,
    };
  }
}
