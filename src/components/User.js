import Client from './Client';

export default class User extends Client {
  constructor(params) {
    super(params);
    this.type = 'User';
  }
}
