export default class Builder {
  constructor(models) {
    this.models = models;
  }
  create(model, ressource) {
    return new this.models[model](ressource);
  }
}
