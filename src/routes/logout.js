const express = require('express');

const router = new express.Router();

router.get('/', (request, response) => {
  request.logout();
  response.redirect('/');
});

export default router;
