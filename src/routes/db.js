import config from '../config/config';
import DataAccessor from '../components/DataAccessor';

const dataAccessor = new DataAccessor(['users', 'clients', 'accesstokens'], config.DB_HOST, true);

const express = require('express');

const router = new express.Router();

router.get('/:model', (request, response) => {
  const model = request.params.model;
  const filters = request.query;
  dataAccessor.search(model, filters, (datas) => {
    response.json(datas);
  });
});
router.post('/:model', (request, response) => {
  const model = request.params.model;
  const type = `${model.toUpperCase()[0]}${model.slice(1, model.length - 1)}`;
  const data = dataAccessor.createRessource(type, request.body);
  dataAccessor.database(model).post(data, (error, datas) => {
    if (error) response.send(error.status, error);
    response.json(datas);
  });
});
router.put('/:model', (request, response) => {
  const model = request.params.model;
  const filters = request.query;
  dataAccessor.search(model, filters, (datas) => {
    if (datas.length === 1) {
      const hydratedBody = datas[0];
      for (const key in request.body) {
        hydratedBody.doc[key] = request.body[key];
      }
      dataAccessor.database(model).put(hydratedBody.doc, (error, datas2) => {
        if (error) response.send(error.status, error);
        response.json(datas2);
      });
    } else {
      response.send(`${model} with name ${filters.name} does not exist`);
    }
  });
});
router.delete('/:model', (request, response) => {
  const model = request.params.model;
  const filters = request.query;
  dataAccessor.search(model, filters, (datas) => {
    if (datas.length === 1) {
      const hydratedBody = datas[0].doc;
      dataAccessor.database(model).remove(hydratedBody, (error, datas2) => {
        if (error) response.send(error.status, error);
        response.json(datas2);
      });
    } else {
      response.send(`${model} with name ${filters.name} does not exist`);
    }
  });
});

export default router;
