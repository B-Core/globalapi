const express = require('express');
const router = new express.Router();

router.get('/', (request, response) => {
  response.render('login');
});

export default router;
