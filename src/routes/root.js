const express = require('express');

const router = new express.Router();

router.get('/', (request, response) => {
  response.send('Welcome to the Echafaudeur.GlobalAPI');
});

export default router;
