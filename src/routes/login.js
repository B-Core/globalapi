const express = require('express');
const passport = require('passport');

const router = new express.Router();


router.get('/', (request, response) => {
  response.render('login');
});

router.post('/', passport.authenticate('local', { successReturnToOrRedirect: '/', failureRedirect: '/login' }));


export default router;
