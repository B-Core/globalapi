import config from '../config/config';

import homeRoot from './root';
import dbRoot from './db';
import loginRoot from './login';
import logoutRoot from './logout';

const morgan = require('morgan')('combined');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors')();
const session = require('express-session')({
  secret: 's3Cur3',
  name: 'sessionId',
  resave: true,
  saveUninitialized: true,
});
const helmet = require('helmet');
const passport = require('passport');
const sassMiddleware = require('node-sass-middleware');

export default (app) => {
  app.use(helmet());
  app.use(morgan);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(cookieParser());
  app.use(sassMiddleware({
    src: 'src/style',
    dest: 'public/style',
    indentedSyntax: true,
    sourceMap: true,
  }));
  app.use(cors);
  app.use(session);
  app.use(passport.initialize());
  app.use(passport.session());

  app.use('/', homeRoot);
  app.use('/login', loginRoot);
  app.use('/logout', logoutRoot);
  // app.get('/dialog/authorize' /* TODO */);
  // app.post('/oauth/token' /* TODO */);
  //
  // app.use(/* TODO authentication test */);
  app.use('/api', dbRoot);

  app.listen(config.APP_PORT, () => {
    console.log(`Server listen on port ${config.APP_PORT}`);
  });
};
