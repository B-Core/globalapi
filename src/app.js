
import router from './routes/router';

const express = require('express');

const app = express();

// view engine setup
app.set('views', 'views');
app.set('view engine', 'hbs');

app.use('/static', express.static('public'));
router(app);

module.exports = app;
